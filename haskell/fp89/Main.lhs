In traditional sense, that's the main program. That's because the
approach we're employing here is basically the following.

Create a Haskell data structure that corresponds to a model of a C
program ("Quasi C") and translate it a string. Each of the strings
that correspond to each part of the program (built this way) is then
printed to the standard output and they form a valid C program
together.

We're building a small language along with its compiler which helps to
automatically generate C code based on our needs. That's a common
practice when using Haskell, since it's a domain-specific language for
creating compilers! Yay!

> module Main where

Add the two main ingredients: the user program and the Quasi C
language definition and its compiler.

> import QuasiC
> import Program

(Ab)use the "main" program to tell the Quasi C compiler which parts of
the user code to translate to C. Note that we don't want to do that
for all of the function definitions there. The ones that aren't
selected here are only metafunctions that help us to generate code.

> main :: IO ()
> main = compile [
>     include interfaceDecl,
>     include table,
>     include counter,
>     include tmp,
>     include vga,
>     include idt,
>     include tick,
>     include start
>   ]
