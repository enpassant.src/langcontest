This is the main source file. It's composed of three parts or layers.

Before getting into anything, here are a couple of minor tweaks for
the compilation. The LANGUAGE pragma is to disable the standard
module, Prelude. Later you'll see that pieces of this module will be
used but with consideration. The OPTIONS_GHC pragram isn't that
important, it just helps to get rid of some annoying warnings.

> {-# LANGUAGE NoImplicitPrelude #-}
> {-# OPTIONS_GHC -fno-warn-missing-methods #-}
> module Program where

These are functions and type classes that we want to use below. Each
of them will be described later in the context where they're used.

> import Prelude (($), (+), (*), (.))
> import Prelude (Functor, Applicative, Monad, (>>=), (*>))
> import Prelude (Int, undefined)

The space starts to bend here: enter the language-oriented
programming. I wouldn't go into the details of LOP but feel free to
read the following paper about it before proceeding:

https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=825a90a7eaebd7082d883b198e1a218295e0ed3b

So, this "Quasi C" module is to implement an embedded domain-specific
language (EDSL for short) to capture the constructs of the C
programming language that we want to use below. See the source code
for the details. In this file, it'll be just shown from the
application side.

> import QuasiC

=== Part 1: C interface ("foreign") definitions

With the help of Quasi C, first we define the C interface functions.
This is done through a special C struct (as it was requested
originally) where all the fields are function pointers. The
"structField" function helps us to create reusable references that can
be turned into callable objects below.

> f_load_idt = structField (ptr (fun void [ptr void, uint16])) (name "load_idt")
> f_enable_interrupts = structField (ptr (fun void [void])) (name "enable_interrupts")
> f_disable_interrupts = structField (ptr (fun void [void])) (name "disable_interrupts")
> f_iretq = structField (ptr (fun void [int])) (name "iretq")
> interfaceDecl =
>   declstruct (name "almost_efi_system_table_but_not_quite_t")
>   [ f_load_idt
>   , f_enable_interrupts
>   , f_disable_interrupts
>   , f_iretq
>   ]

Besides the struct declaration, there we add the variables. They're
created with the "var" function which needs the type and the name.
The last part is the initial value, which might be missing. In those
cases, simply "none" is mentioned.

> interface = asType interfaceDecl
> table = var (ptr interface) (name "table") none
> counter = var uint32 (name "counter") (some (uint32Literal 0))
> tmp = var uint16 (name "tmp") none
> vga = var (ptr uint8) (name "vga") (some (ptrLiteral uint8 0xB8000))
> idt = var (array uint64 (48 * 2)) (name "idt") none

These are extra metavariable to make it easier working with the
function pointers that are buried in the C struct that describes the
interface.

> ifLoadIDT = asFuncDecl table f_load_idt
> ifEnableInterrupts = asFuncDecl table f_enable_interrupts
> ifIretq = asFuncDecl table f_iretq

=== Part 2: Abstraction layer on top of the C interface

Now the following definitions can be considered as an embedded
library. They wrap the elements of the C interface to distance the
user code from the low-level details.

The "mkIdtEntry" function is responsible for building a 64-bit machine
word from the interrupt handler's address and the various
configuration parameters. We could have gone with the struct-based
approach here, but for some reason, the C compilers aren't so good at
optimizing those. See the definition of the "idt" variable above which
nicely aligns with this.

> mkIdtEntry (func, selector, ist, flags) =
>   ((address .&. complement 0xFFFF) << 32) .|.
>   ((uint64Literal flags) << 40) .|.
>   ((uint64Literal ist) << 32) .|.
>   ((uint64Literal selector) << 16) .|.
>   (address .&. 0xFFFF)
>   where
>     address = addressOf func

The "setupInterruptHandler" function here encapsulates the creation
and addition of the IDT entry, then loads the whole table by calling
the corresponding interface routine.

> setupInterruptHandler (selector, func) = do {
>     idt%@(selector * 2) %= mkIdtEntry(func, selector, 0, 0x8E);
>     call ifLoadIDT [ref idt, uint16Literal $ 48 * 16]
>   }

Some additional convenience wrappers to give a better look & feel for
the user code.

> enableInterrupts = call ifEnableInterrupts []
> halt = while (true) emptyBody
> iretq n = call ifIretq [intLiteral n]

The next big thing here is the monadic (yikes!) text screen handling
abstraction. Fear not, I'll try to explain what's going on here! B-)

By defining the "TextIO" type, we create a subset of the traditional
I/O routines that'd focus on working with the text screen mode. More
specifically, it focuses on printing characters. This is achieved by
manipulating the video memory through the "vga" Quasi C variable
above. "TextIO" is used to keep a state around with the location of
the cursor and its color. That is, on "printing" each character or
string, we'll just advance the cursor (the pointer in the video
memory) while setting the character's code there.

In parallel to all these, "TextIO"'s going to be special because it
won't do anything directly but glue together a Quasi C program for the
given task. Yes, yes, it's a metaprogram!

> newtype TextIO a
>   = TIO ((Color, Color, Int, CFuncBody ()) -> (Color, Color, Int, CFuncBody ()))

Promote "TextIO" to a monad through the Functor-Applicative-Monad
stack.  These details are purely technical, don't worry if they don't
make any sense to you. The goal here is to let "TextIO" use the "do"
syntax for the code blocks.

For the curious ones, here are more details.

"TextIO" wraps a state transition function that carries the foreground
& background colors, the current position on the screen and the
generated program up to that point. Each "TextIO" operation is
basically another state transformer that modifies some part of the
state that is passed over. The semicolon in the "do" block becomes the
"*>" operator where the task is to glue the chain of state
transformers by mathematical function composition (the "."
operator"). Ideally, this'd be enough, but for some reason GHC insists
on using the ">>=" operator. Hence a fake definition is added to
gently make it use the "*>" operator instead.

This could have been expressed as a State and Writer stack, but I
didn't want to complicate it further.

> instance Functor TextIO
> instance Applicative TextIO where (TIO f) *> (TIO g) = TIO (g . f)
> instance Monad TextIO where x >>= k = x *> (k undefined)

A separate abstraction to describe colors on the text
screen. Currently there are only black and grey supported. "None" is a
special extra value to tell that "don't modify the color".

> data Color = None | Black | Grey
>
> black = Black
> grey = Grey
>
> colorCode Black = 0
> colorCode Grey = 7
>
> color tc bc = (colorCode bc) * 16 + (colorCode tc)

The "onTextScreen" function becomes a keyword to tell that the
succeeding block should be interpreted as a series of actions on the
text screen. Therefore it becomes another EDSL on its own. Fun!

> onTextScreen (TIO tio) = body
>   where (_, _, _, body) = tio (None, None, 0, emptyBody)

Here are the actions that are allowed to use in a "TextIO" code block.
First, setting the text foreground color:

> setTextColor tc = TIO $ \(_, bc, idx, code) -> (tc, bc, idx, code)

Setting the text background color:

> setBackgroundColor bc = TIO $ \(tc, _, idx, code) -> (tc, bc, idx, code)

Putting an individual character to the cursor's current position and
move the cursor one character ahead:

> printChar x = TIO $ \(tc, bc, idx, code) ->
>   (tc, bc, idx + 2, do {
>      code;
>      vga%@(idx) %= x;
>      case (tc, bc) of
>        (None, None) -> emptyBody;
>        _            -> vga%@(idx + 1) %= uint8Literal (color tc bc)
>   })

The "print" function here is an extension to "printChar" to make it
span over strings. This happens by joining the smaller "printChar"
programs into a single one to form the code for "print":

> print xs = join [printChar x | x <- xs]

A few decorator functions ("plugins") to make "print" work with
regular Haskell string literals and sequences of numbers to be treated
as digits:

> string s = [charLiteral c | c <- s]
> digits ns = [plus n (charLiteral '0') | n <- ns]

=== Part 3: Top-level user code

OK, so let's see what we've done here. Now we have all the abstraction
to author the user code >without< losing any performance.

"tick" is our interrupt handler, which is defined as a Quasi C
function via "defun". After specifying the return type, the name, the
list of arguments, the "$" operator introduces the body, presented as
a "do" block.

Thanks to Quasi C, the "do" block is now interpreted as a Quasi C
program with its own abstractions. Through the various Quasi C
construct, it should be easy to read the definition. All of them does
what you'd expect.

> tick = defun void (name "tick") [] $ do {
>     increment counter;
>     when (counter % 100 == 0) $ do {
>         tmp %= (counter / 100) % 10000;
>         let factors = [
>               tmp / 1000,
>               (tmp % 1000) / 100,
>               (tmp % 100) / 10,
>               tmp % 10] in
>         onTextScreen $ do {
>            (print.digits)(factors)
>         }
>     };
>     iretq 0
>   }

"start" is the main program. Similarly to "tick", the definition does
what each of the methods suggests there below.

> iface = (ptr interface, name "iface")
> start = defun void (name "_start") [iface] $ do {
>     table %= ref (asVar iface);
>     onTextScreen $ do {
>       setTextColor(grey);
>       setBackgroundColor(black);
>       (print.string)("0000")
>     };
>     setupInterruptHandler(32, tick);
>     enableInterrupts;
>     halt;
>   }

That's all folks!
