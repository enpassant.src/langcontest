This is the definition of the "Quasi C" embedded domain-specific
language (EDSL) and its compiler. It's named after the fact that it
aims to model a rather restricted subset of the C programming
language. That said, the functions and the types below are to express
the syntax and they intentionally resemble to their counterparts in C.
Each function is used to build abstract syntax tree (AST) of the Quasi
C program. These are then taken by the compiler and translated to a
string which represents the corresponding C program. This way we can
guarantee that only well-formed C programs are created through
Haskell's type system. Well, that's the essence, but keep on reading
for more!

The LANGUAGE pragma here enables the "Generalized Algebraic Data
Types" extension over the Haskell 98 standard. This is used for the
definition of the "CFuncBody" type (see below) to implement some
polymorphism for the data constructors. The OPTIONS_GHC pragma is used
to disable some annoying warnings, it's not required.

> {-# LANGUAGE GADTs #-}
> {-# OPTIONS_GHC -fno-warn-missing-methods #-}

This is the module interface, that is, the list of all the language
elements that can be used to build Quasi C programs. We won't
elaborate on those here but down there at the respective definitions.
But they're meant to be descriptive.

> module QuasiC (
>   name,
>   void, array, uint8, uint16, uint32, uint64, int, fun, ptr,
>   declfun, defun, var, declstruct, structField, some, none,
>   asType, asVar, asFuncDecl,
>   addressOf, (%=), (%@), (.&.), (.|.), (<<), (>>), (/), (%), (==), plus,
>   ref, true, false, complement,
>   intLiteral, uint8Literal, uint16Literal, uint32Literal, uint64Literal,
>   charLiteral, ptrLiteral,
>   CFuncBody, call, increment, while, when, join, emptyBody,
>   compile, include
> ) where

These are the standard Haskell modules that we use for the
implementation. They're all shipped with the Glasgow Haskell Compiler.
Since some of the definitions will be replaced ("reinterpreted") in
this module, they're hidden. Fortunately(?), Haskell lets you to
redefine many of its basic functions.

> import Prelude hiding ((==), (/), (>>))
> import Control.Monad hiding (void, when, join, (>>))
> import Data.Char
> import Data.List
> import GHC.Integer
> import Text.Printf

This function is about the creation of a C identifier. We can't let
any Haskell string to be used as a name, they have to comply to the
expectations of the language. For now, these checks are missing
(oops!) but they can be added easily as part of the "name" function.

This pair of definitions introduces a pattern for the rest of the
module. First, there's the type itself, "CName" in this case, which is
then followed by a constructor that can be used by the user to
construct values of this type. "CName" is derived from Haskell's
"String" type hence both the definitions are rather simple.

There we tell the Haskell compiler to make the type part of the "Show"
type class. That's important because we want to translate each of
these types to strings. Type classes in Haskell represent overloading
of names, that is, they make it possible to use the same name with
multiple type signatures. Here we basically define a "show" function
instance with the type "CName -> String".  Thanks to these extensions,
later the compiler will be able to tell by itself which instance to
use at the given call site based on the types only. Cool stuff!

> newtype CName = CN String
>
> name = CN
>
> instance Show CName where show (CN name) = name

The "CType" type captures the C types that we want to make accessible
in our Quasi C dialect. The "data" keyword below is used to enumerate
all the elements. The constructor functions are bound each of the
cases. "ptr" is special, because it takes a parameter, the type that
it'd create a pointer for. "array" is similar to "ptr", but it works
with type of the elements and a size, and it creates a C array
definition. "fun" wraps a function pointer and "struct" wraps a C
struct.

> data CType
>   = Void | Int | UInt8 | UInt16 | UInt32 | UInt64
>   | Fun CType [CType]
>   | Array CType Int
>   | Struct CName
>   | Ptr CType
>
> void = Void
> array = Array
> uint8 = UInt8
> uint16 = UInt16
> uint32 = UInt32
> uint64 = UInt64
> int = Int
> ptr = Ptr
> fun = Fun
>
> instance Show CType where
>   show Void = "void"
>   show Int = "int"
>   show UInt8 = "uint8_t"
>   show UInt16 = "uint16_t"
>   show UInt32 = "uint32_t"
>   show UInt64 = "uint64_t"
>   show (Struct n) = show n
>   show (Ptr t) = printf "%s *" (show t)

"decfun" can be used to compose C function declarations. Much like
its C counterpart, it needs a return type, a name, and list of
parameters. As expected, the names and types require the use of the
"CName" and "CType" types from the above. The parameters are described
by pairs of their names and types.

The "CFunc" type is basically a 4-tuple. In Haskell, the "data"
keyword can be used to form tuples like that by stitching the types
for the components together. The first name on the right-hand side is
the name of the data constructor itself.

> data CFunc = CFunc CType CName [(CType, CName)] (Maybe (CFuncBody ()))
>
> declfun typ name params = CFunc typ name params Nothing

"defun" is similar to "declfun", but it expects a function body as
well. We'll talk about that one below.

> defun typ name params body = CFunc typ name params (Just body)

C structs can be defined with the help of the "declstruct" and
"structField" constructors. They're each backed up by the "CStruct"
and "CStructField" types. That latter helps storing some additional
metainformation about the fields that can be used in certain cases.

> data CStructField = CStructField CType CName
> data CStruct = CStruct CName [CStructField]
>
> declstruct = CStruct
> structField = CStructField

The "asFuncDecl" function helps to create function 'objects' from
fields in C structs. Note that similar wrappers can be added since
many other kind of C expressions can be turned into functions that we
can call. Currently, there aren't any other cases to worry about, so
the implementation is kept short.

> asFuncDecl (CVar _ name _) (CStructField (Ptr (Fun rt pts)) field) =
>   CFunc rt accessor params Nothing
>   where
>     params   = zip pts [ CN (printf "arg%d" i) | i <- [(0 :: Int)..] ]
>     accessor = CN $ printf "%s->%s" (show name) (show field)

The "asType" function gives us a projection of the C struct definition
as a simple type, just to be able to fit to the right places.

> asType (CStruct name _) = Struct name

"var" can be used to declare C variables. They have a type and a name,
as it's for the functions. The third parameter is the initial value,
which can be missing actually. There we use Haskell's "Maybe" type.
For the easier use, the "some" and "none" wrappers are
added. Basically, "none" means there's no initial value. "some" lets
the user to specify a C expression, covered by the "CExpr" type, as
the initial value.

When "show"-ing a variable, its name is extracted by pattern matching.
This language construct helps to mask out all the unneeded components
of the "CVar" definition and make a reference to the one we need. The
two underscores stand for the type and initial value, and their
meaning is to "ignore these for now".

> data CVar = CVar CType CName (Maybe CExpr)
>
> var = CVar
> some = Just
> none = Nothing
>
> instance Show CVar where show (CVar _ name _) = show name

Variables can be created on the spot from a pair of a type and a name,
so they aren't required to be defined upfront.

> asVar (typ, name) = CVar typ name Nothing

The "CExpr" type is one of the main components of this module. It
tries to model the expressions in the C language. One kind of
exprexsion are the literals and here we declare the "IntWidth" and
"IntType" auxiliary types that can help us to talk about integer
literals, their signs and sizes.

> data IntWidth = IMachineWord | I8 | I16 | I32 | I64
> data IntType = Signed | Unsigned

Besides literals there can be variable references, array indexing,
function address calculation, various bit-level operations, and
regular arithmetic operations, equality checking. All of them are
assigned with a data constructor. This constructor basically sticks
the token of the operator with subtrees of the expression that are its
operands.

> data CExpr
>   = CLitInt IntType IntWidth Int
>   | CLitBool Bool
>   | CLitPtr CType Int
>   | CVarRef CVar
>   | CArrIdx CName Int
>   | CFuncAddr CName
>   | BitAnd CExpr CExpr
>   | BitOr CExpr CExpr
>   | BitShift Int CExpr
>   | Plus CExpr CExpr
>   | DivBy CExpr CExpr
>   | ModBy CExpr CExpr
>   | Equals CExpr CExpr

The instance definition of the "show" function for the "CExpr" type
takes care of translating each of the possible constructs to a string,
that is, a C program. Here we commonly use the Haskell version of the
"printf" function which works similarly to its C counterpart.

> instance Show CExpr where
>   show (CLitInt Unsigned I64 n) = printf "%sUL" (show n)
>   show (CLitInt _ _ n) = show n
>   show (CLitBool True) = "1"
>   show (CLitBool False) = "0"
>   show (CLitPtr typ ptr) = printf "(%s *) 0x%08X" (show typ) ptr
>   show (CVarRef ref) = show ref
>   show (CArrIdx name x) = printf "%s[%s]" (show name) (show x)
>   show (CFuncAddr name) = printf "(uintptr_t) %s" (show name)
>   show (Plus n1 n2) = printf "(%s) + (%s)" (show n1) (show n2)
>   show (BitAnd b1 b2) = printf "(%s) & (%s)" (show b1) (show b2)
>   show (BitOr b1 b2) = printf "(%s) | (%s)" (show b1) (show b2)
>   show (BitShift n b)
>     | n < 0 = printf "(%s) >> %s" (show b) (show (abs n))
>     | n > 0 = printf "(%s) << %s" (show b) (show n)
>     | otherwise = show b
>   show (DivBy x y) = printf "(%s) / (%s)" (show x) (show y)
>   show (ModBy x y) = printf "(%s) %% (%s)" (show x) (show y)
>   show (Equals x y) = printf "(%s) == (%s)" (show x) (show y)

We add a "ToCExpr" class to simplify the creation of simulated C
expressions and apply the right translation for the right types by
overloading the "toCExpr" function.

> class ToCExpr a where toCExpr :: a -> CExpr
> instance ToCExpr CExpr where toCExpr = id
> instance ToCExpr Int where toCExpr = intLiteral
> instance ToCExpr CVar where toCExpr = CVarRef

The "addressOf" function wraps the function address calculation
operation and introduces its own token in the tree. So that a
specialized translation can be performed above.

> addressOf (CFunc _ name _ _) = CFuncAddr name

The "%@" symbol lets us to talk about array indexing. At the moment it
isn't checked (oops!) if the variable reference on the left-hand side
is actually an array or the integer that stands in as index is out of
bounds of the array. But this can be added with some effort.

> infix 7 %@
>
> (CVar _ name _) %@ idx = CArrIdx name idx

The "ref" function explicitly helps to clarify that we mention a name
a reference to a variable. Something this is needed due to technical
reasons.

> ref = CVarRef

Our Boolean literals are "true" and "false".

> true = CLitBool True
> false = CLitBool False

Integer literals of various kinds.

> intLiteral = CLitInt Signed IMachineWord
> uint8Literal = CLitInt Unsigned I8
> uint16Literal = CLitInt Unsigned I16
> uint32Literal = CLitInt Unsigned I32
> uint64Literal = CLitInt Unsigned I64

Character literals, which are basically 8-bit integer literals. They
can be built from Haskell characters (the "Char" type"), that's why
the "ord" function is used to perform the required transformation.

> charLiteral = uint8Literal . ord

Pointer literals where we need tell the type what it points to,
besides the address.

> ptrLiteral = CLitPtr

Infix bit-level operators: ".&." is for bitwise inclusive AND, and
".|." is for bitwise OR.

> infixl 7 .&.
> infixl 5 .|.
>
> x .&. y = BitAnd x (uint64Literal y)
> (.|.) = BitOr

Bitshifting operations to left ("<<") and the right (">>"). They are
expressed by the same token in the tree but with a different
parametrization.

> x << n = BitShift n x
> x >> n = BitShift (-n) x

"complement" is only a technical conversion function, in case we
wanted to compute the bitwise complement of a literal.

> complement :: Int -> Int
> complement = fromIntegral . complementInteger . fromIntegral

Some of the basic arithmetic operations below are intentionally made
overlapping with the ones that are present in Haskell's standard
library, the Prelude module. The reason is to make it easier for the
Quasi C program author to use these operators as they were regular
operators, while actually producing "CExpr" values instead of
evaluating them when running the code.

> infixl 7 /
> infixl 7 %
> infix 4 ==
>
> class COps a where
>   (/) :: a -> Int -> CExpr
>   (%) :: a -> Int -> CExpr
>   (==) :: a -> Int -> CExpr

The "plus" wasn't made member of this club for now.

> plus = Plus

It's visible that overloading of the operators is happening only in
the first operand. The second operand is always assumed to be a
Haskell integer, for the sake of simplicity. This way we can save some
typing for the user, but probably it's a very use-case-specific and
less future-proof solution.

> instance COps CExpr where
>   x / y = DivBy x (intLiteral y)
>   x % y = ModBy x (intLiteral y)
>   x == y = Equals x (intLiteral y)

Since variables are often used in arithmetic expressions, it becomes
handy to overload them for automated conversions between variable
names and references.

> instance COps CVar where
>   x / y = DivBy (CVarRef x) (intLiteral y)
>   x % y = ModBy (CVarRef x) (intLiteral y)
>   x == y = Equals (CVarRef x) (intLiteral y)

The other main component of this module is the "CFuncBody" type. This
is to represent a C function body. The attack is carried out from two
sides. The data constructors of "CFuncBody" stand for the possible
types of statements that may occur in the body. More specifically,
there can be empty bodies ("CFBEmpty"), function calls ("CFBCall"),
assignments ("CFBAssg"), incrementing the value of variable
("CFBIncrement""), a "while" loop ("CFBWhile"), a one-arm branching
("CFBWhen"), and the semicolon ("CFBSeq").

The set of constructors can be extended further. But we're happy with
this now.

> data CFuncBody a where
>   CFBEmpty :: CFuncBody a
>   CFBCall :: CFunc -> [CExpr] -> CFuncBody a
>   CFBAssg :: CExpr -> CExpr -> CFuncBody a
>   CFBIncrement :: CVar -> CFuncBody a
>   CFBWhile :: CExpr -> CFuncBody a -> CFuncBody b
>   CFBWhen :: CExpr -> CFuncBody a -> CFuncBody b
>   CFBSeq :: CFuncBody a -> CFuncBody b -> CFuncBody c

The Functor-Applicative-Monad instance definitions help us to overload
the "do" syntax and use the "CFBSeq" constructor to connect two C
function body statement sequentially. This is marked by the ";" symbol
in C which is kind of a delimiter, but it's an operator here! With
this approach, it's allowed to build function bodies by sequentially
composing other function bodies.

> instance Functor CFuncBody
> instance Applicative CFuncBody where (*>) = CFBSeq
> instance Monad CFuncBody where x >>= k = x *> (k undefined)

"call" wraps a function call to either an internally or an externally
defined function.

> call = CFBCall

The "increment", "while" and "when" functions simply wrap the
corresponding data constructors. "increment" needs only a variable
reference. "while" works with a C expression as the loop condition and
a C function body as the loop body. "when" is like "while" but it
won't run the body multiple times but once, when the condition
evaluates to true.

> increment = CFBIncrement
> while = CFBWhile
> when = CFBWhen

The assignment is wrapped as an operator. Through overloading, it can
work with either variable names or expressions. We need that latter
because sometimes names themselves can be transformed further. For
example, if the assignment applies to an array element of a given
index.

  (arr %@ i) %= 42

> infix 5 %=
>
> class CAssg a where (%=) :: a -> CExpr -> CFuncBody ()
> instance CAssg CVar where name %= x = CFBAssg (CVarRef name) x
> instance CAssg CExpr where expr %= x = CFBAssg expr x

"join" is a technical function that can be used to stick multiple "do"
blocks together when needed.

> join :: Applicative a => [a ()] -> a ()
> join = foldr1 (*>)

This is the representation empty C function body. It's not guaranteed
that it'll show up anywhere explicitly in the translated code, but
it's needed for the internal model.

> emptyBody = CFBEmpty

Finally, the "compile" function is responsible for building the string
from the Quasi C programs and send it to the standard output. Of
course, it could have sent to a different location (something
parametric), but for this purpose it works just well.

> compile cdecls = do
>   printf "#include <stdint.h>\n"
>   mapM_ compileCDecl cdecls
>
> data CDecl
>   = CDeclF CFunc
>   | CDeclV CVar
>   | CDeclS CStruct

The "include" helper function can be used to decide which definitions
in the program should be made part of the resulting generated C
program. This is because sometimes the user code wants to define code
generator functions in Haskell which are used implicitly to create the
ones that'll be compiled.

> class Include a where include :: a -> CDecl
> instance Include CFunc where include = CDeclF
> instance Include CVar where include = CDeclV
> instance Include CStruct where include = CDeclS

"compileDecl" shows how to generate the C program that corresponds to
each kind of the Quasi C declarations.

> compileCDecl (CDeclF (CFunc typ name params Nothing)) =
>   printf "%s %s(%s);\n" (show typ) (show name) (showParams params)
>
> compileCDecl (CDeclV (CVar (Array typ size) name init)) =
>   printf "%s %s[%s]%s;\n" (show typ) (show name) (show size) (initVal init)
>
> compileCDecl (CDeclV (CVar typ name init)) =
>   printf "%s %s%s;\n" (show typ) (show name) (initVal init)
>
> compileCDecl (CDeclS (CStruct name fields)) =
>   printf "typedef struct {%s;} %s;\n" fieldsStr (show name)
>   where
>     fieldsStr = intercalate "; " $ map asStr fields
>     asStr (CStructField (Ptr (Fun rt pts)) name) =
>       let paramsStr = intercalate ", " $ map show pts in
>       printf "%s (*%s)(%s)" (show rt) (show name) paramsStr
>     asStr (CStructField typ name) = printf "%s %s" (show typ) (show name)
>
> compileCDecl (CDeclF (CFunc typ name params (Just body))) = do
>   printf "%s %s(%s) {" (show typ) (show name) (showParams params)
>   compileBody body
>   printf ";}\n"
>
> showParams []     = "void"
> showParams params = intercalate ", " . map translate $ params
>   where
>     translate (t, n) = printf "%s %s" (show t) (show n)
>
> initVal Nothing  = ""
> initVal (Just x) = printf " = %s" (show x)

"compileBody" shows how the Quasi C function bodies are translated to
C programs.

> compileBody :: CFuncBody a -> IO ()
> compileBody (CFBCall ref params) = printf "%s(%s)" (show name) paramStr
>   where
>     paramStr = intercalate ", " $ map show params
>     CFunc _ name _ _ = ref
>
> compileBody (CFBIncrement name) = printf "%s++" (show name)
> compileBody (CFBAssg name value) = printf "%s = %s" (show name) (show value)
> compileBody (CFBWhile cond CFBEmpty) = printf "while (%s)" (show cond)
> compileBody (CFBWhen cond body) = do
>   printf "if (%s) {" (show cond)
>   compileBody body
>   printf ";}"
>
> compileBody (CFBSeq CFBEmpty c) = compileBody c
> compileBody (CFBSeq c CFBEmpty) = compileBody c
> compileBody (CFBSeq c1 c2) = do
>   compileBody c1
>   printf ";"
>   compileBody c2
